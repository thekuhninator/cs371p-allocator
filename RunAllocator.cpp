// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <vector>
#include <string> // getline, string
#include "Allocator.hpp"


// ----
// main
// ----
bool debug2 = false;

void solve_allocator(std::vector<int> allocateRequests) {
    // let's create a new allocator
    my_allocator<double, 1000> all;
    // iterate through the calls..
    // let's keep track of all the pointers in a vector
    std::vector<double*> pointers;
    for (int& req : allocateRequests) {
        // if we are allocating something
        if(req >= 0) {
            all.allocate(req);
        }
        else {
            all.freeNthBlock(req);
        }
        if(debug2)
        {
            cout << "REQUEST " << req << endl;
            all.printMem();
        }
    }
    all.printMem(); // print the memory
}



int main() {
    using namespace std;

    // Get number of tests
    int numTests;
    cin >> numTests;

    // garbage day...
    string garbage;
    getline(cin, garbage);
    getline(cin, garbage);

    // for all the tests...
    for(int i = 0; i < numTests; i++)
    {
        vector<int> allocateRequests; // allocation requests...
        string line;
        getline(std::cin, line);

        while(!line.empty())
        {
            // parse the int...
            int request = stoi(line);
            // let's add it to a vector...
            allocateRequests.push_back(request);
            getline(cin, line);
            // then let's send it to to the solve...
        }
        // let's solve it...
        solve_allocator(allocateRequests);
    }

    return 0;
}