// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------
using namespace std;
#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

bool debug = false;

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            // for this we should return the value at p probabky...
            return *_p;
        }

        // -----------
        // operator ++
        // -----------
        iterator& operator ++ () {
            char* newP = (char*)(_p);
            // let's move newP
            newP = newP + 8 + (abs(*_p)); // sentential + 4 bytes + number of bytes * size
            // let's print out the new pointer
            _p = (int*)(newP);
            return *this;
        }

        int* getPointer()
        {
            return _p;
        }
        // merge the two ends...
        iterator merge(int* prevStart, int* currStart, int* currEnd)
        {
            if(debug) cout << "in merge function these are the sentineials... " << endl;
            if(debug) cout << *prevStart << " | curr Start: " << *currStart << " | curr end:  " << *currEnd << endl;

            int firstAmount = abs(*prevStart);
            int secondAmount = abs(*currStart);
            int newAmount = firstAmount + secondAmount + 8;
            *prevStart = newAmount;
            *currEnd   = newAmount;
            return iterator(prevStart);
        }
        // merge from the left
        iterator mergeLeft()
        {
            // let's merge the left and the right...
            int* prevStart = getPrev(_p);
            if(*prevStart < 0) // if it is busy we can't merge
                return *this;
            if(debug)
                if(debug) cout << "we are going to merge left!" << endl;
            // first let's get the pointer to the left start...
            int* currStart = _p;
            int* currEnd = getEnd(currStart);
            // so we want to merge them which looks like...
            return merge(prevStart, currStart, currEnd);
        }
        // merge them from the right...
        iterator mergeRight()
        {
            int* nextStart = getNext(_p);
            if(*nextStart < 0)
                return *this;
            if(debug) cout << "we are going to merge right! " << endl;
            int* currStart = _p;
            int* nextEnd   = getEnd(nextStart);

            return merge(currStart, nextStart, nextEnd);
        }

        // -----------
        // operator ++
        // -----------

        void setBusy(int numBlocks) {
            // there is one last thing we have to do in setbusy
            // that is to uhhhh check if there is memory with a little something in it...

            int numBytes = numBlocks * sizeof(T);
            // we need to have two cases...
            // 1 normal case
            // one case where it fits perfectly...
            // it will fit perfectly if number of blocks requested equals *_p
            if(*_p == numBytes)
            {
                // then we are in easy street...
                char* endFirstBlockChar = (char*)(_p);
                endFirstBlockChar = endFirstBlockChar + 4 + numBlocks;
                int* endBlock = (int*) endFirstBlockChar;
                *_p = -numBytes;
                *endBlock = -numBytes;
                // we are done
                return;
            }

            int originalBytes = *_p;
            // so given a block we are currently at...
            char* newP = (char*)(_p);
            newP = newP + 4 + (*_p); // sentential + 4 bytes + number of bytes * size
            char* endFirstBlock = (char*)(_p);
            endFirstBlock = endFirstBlock + 4 + numBytes;
            char* endSecondBlock = endFirstBlock + 4;
            // let's print out the new pointer
            int* firstBegin = _p;
            int* firstEnd = (int*)(endFirstBlock);
            int* secondBegin = (int*)(endSecondBlock);
            int* secondEnd   = (int*)(newP);
            // original 40 - 24 = 16 - 8 = 8
            int leftOverBytes = originalBytes - numBytes - 8;

            // first we need to check if leftOverBytes is enough to form one...
            if(leftOverBytes >= (int)(sizeof(T)))
            {
                *firstBegin = -numBytes;
                *firstEnd = -numBytes;
                *secondBegin = leftOverBytes;
                *secondEnd = leftOverBytes;
            }
            else
            {
                // otherwise we need to make the first begin be the entire size
                *firstBegin = -abs(originalBytes);
                *secondEnd =  -abs(originalBytes);
            }
        }
        // get the previous pointer from a pointer...
        int* getPrev(int* loc)
        {
            loc = loc - 1;
            // read this value... that's how many bytes we have to go back...
            char* newP = (char*)(loc);
            newP = (newP - abs(*loc)) - 4; // should probably get absoluute value...
            return (int*)(newP);
        }
        // get the next sentineial from a sentineial...
        int* getNext(int* loc)
        {
            // the next would be
            char* newP = (char*)(loc);
            newP = newP + 8 + (abs(*_p));
            return (int*)(newP);

        }
        // get the end block of a sentinial from a beggining sentinaial
        int* getEnd(int* loc)
        {
            char* endChar = (char*)(loc);
            endChar = endChar + 4 + abs(*loc);
            return (int*)(endChar);
        }
        // free a block...
        void freeBlock()
        {
            // let's just free this block bro...
            int* begin = _p;
            int* end   = getEnd(_p);

            // let's free these two...
            *begin = abs(*begin);
            *end   = abs(*end);
        }

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            // we need to update p...
            // take p back one
            _p = _p - 1;
            // read this value... that's how many bytes we have to go back...
            char* newP = (char*)(_p);
            newP = (newP - abs(*_p)) - 4; // should probably get absoluute value...
            _p = (int*)(newP);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return (lhs._p == rhs._p);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            // here we cna probably just get teh value of the pointer...
            return *_p;
        }
        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            char* newP = (char*)(_p);
            // let's move newP
            newP = newP + 8 + (abs(*_p)); // sentential + 4 bytes + number of bytes * size
            // let's print out the new pointer
            _p = (int*)(newP);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            _p = _p - 1;
            // read this value... that's how many bytes we have to go back...
            char* newP = (char*)(_p);
            newP = (newP - abs(*_p)) - 4; // should probably get absoluute value...
            _p = (int*)(newP);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Iterate through the entire array and make sure it sums up to N
     */
    bool valid () const {
        if(debug) cout << " in the valid functoin " << endl;
        const_iterator b = this->begin();
        const_iterator e = this->end();
        int total = 0;
        while(b != e)
        {
            int blockValue = *b;
            if(blockValue == 0)
                return 0;
            else
                total += abs(blockValue) + 8;
            b++;
        }
        return total == N;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // let's throw a bad alloc if we should
        if(N < sizeof(T) + (2 * sizeof(int)))
            throw bad_alloc();

        // set the first sentineial to the size given...
        if(debug) cout << "we are setting the first and last sentinial to " << N - 8 << endl;
        (*this)[0]     = N - 8;
        (*this)[N-4]   = N - 8;
        if(debug) cout << "the address of the FIRST sentinential " << &((*this)[0]) << endl;
        if(debug) cout << "the address of the LAST sentinential " << &((*this)[N-4]) << endl;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;


    // --------
    // allocate
    // --------
    void freeNthBlock(int n)
    {
        iterator it = getNthBusy(n);
        deallocate((pointer)(it.getPointer() + 1), sizeof(T));
    }

    void printMem()
    {
        // print all the blocks!
        iterator b = getBeginIterator();
        iterator e = getEndIterator();
        int i = 0;
        while(b != e)
        {
            if(i != 0)
                cout << " ";
            i++;
            cout << *b;
            b++;
        }
        cout << endl;
    }

    iterator getNthBusy(int n)
    {
        // get the nth busy block and return the iterator pointing to it...
        n = abs(n);
        // let's iterate until we find the nth busy block...
        iterator b = this->getBeginIterator();
        iterator e = this->getEndIterator();
        while(b != e)
        {
            if(*b < 0) // if the block is busy
            {
                n--;
                if(n == 0)
                    return b;
            }
            b++;
        }
        return nullptr;
        // if we made it here something bad happened...
    }
    // get the first free block
    iterator getFirstFree(int sizeB)
    {
        int sizeRequired = sizeof(T) * sizeB;
        iterator it = getBeginIterator();
        iterator e  = getEndIterator();
        while(it != e) {
            // check if the block is free
            if(debug) cout << "searching for a freee block of size " << sizeB * sizeof(T);
            if(debug) cout << "this block has size " << *it << endl;

            if(*it >= sizeRequired) {
                if(debug) cout << "we found a block!!! " << endl;
                return it; // then it is free and we should return it...
            }

            else
                ++it; // otherwise let's continue
        }
        // if we make it to the end then there was nothing there we should throw badalloc?
        throw bad_alloc();
        return nullptr;
    }
    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type b) {
        if(debug) cout << "Before allocate " << endl;
        if(debug) printMem();
        // get the first free block
        iterator it = getFirstFree(b);
        // set it to be not free....
        it.setBusy(b);
        // we need to get the beggining of that block and end of that block...
        // create a new block right next to it...
        if(debug) cout << "after allocate " << endl;

        if(debug) printMem();

        assert(valid());
        return (pointer)(it.getPointer() + 1);
    }             // replace!

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * Deallocate the pointer from free using helper methods and merge afterwards...
     */
    void deallocate (pointer freeP, size_type) {
        // <your code>

        // here we need to free the block...
        iterator it = iterator((int*)(freeP) - 1);
        it.freeBlock();
        // then we need to check if can merge the previous block
        if(debug) cout << "after freeing block " << endl;
        if(debug) printMem();
        iterator left = it;
        // let's double check this code yo...
        if(it != getBeginIterator()) {
            if(debug) cout << "we can merge left " << endl;
            left = it.mergeLeft();
        }
        if(debug) cout << "after merging left.. " << endl;
        if(debug) printMem();
        // I think we really have to be careful about the end iterator...
        iterator next = left;
        next++;
        if(next != getEndIterator()) {
            if(debug) cout << "we can merge right " << endl;
            if(debug) cout << "this is what the blocks look like " << endl;
            if(debug) printMem();
            if(debug) cout << "this is the current block " << *it << endl;
            left.mergeRight();


        }
        if(debug) cout << "after deallocate " << endl;
        if(debug) printMem();
        // then check if we can merge the next block...

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    iterator getBeginIterator () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    iterator getEndIterator () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
